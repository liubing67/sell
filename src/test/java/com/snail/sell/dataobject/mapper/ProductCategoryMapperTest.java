package com.snail.sell.dataobject.mapper;

import com.snail.sell.dataobject.ProductCategory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:17 2017/12/23
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductCategoryMapperTest {

    @Autowired
    private ProductCategoryMapper productCategoryMapper;
    @Test
    public void insertByMap() throws Exception {

        Map<String,Object> map = new HashMap<>();
        map.put("category_name","圣诞热销");
        map.put("category_type",101);
        int result = productCategoryMapper.insertByMap(map);
        Assert.assertEquals(1,result);
    }

    @Test
    public void selectByCategoryType(){
        ProductCategory result = productCategoryMapper.selectByCategoryType(101);
        Assert.assertNotNull(result);
    }

}