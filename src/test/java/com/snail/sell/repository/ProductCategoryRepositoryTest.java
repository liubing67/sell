package com.snail.sell.repository;

import com.snail.sell.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:16 2017/12/2
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepositoryTest {
    @Autowired
    private ProductCategoryRepository repository;
    @Test
    public void findOneTest(){
        ProductCategory productCategory = repository.findOne(1);
        System.out.println("+++++++++++"+productCategory);
    }

    @Test
    public void addOneTest(){
        ProductCategory productCategory = new ProductCategory("男1生最爱",4);

       ProductCategory result =  repository.save(productCategory);
        Assert.assertNotNull(result);
    }
    @Test
    public void updateOneTest(){
        ProductCategory productCategory = repository.findOne(2);
        /*productCategory.setCategory_name("男生最爱");*/

        productCategory.setCategoryType(5);
        repository.save(productCategory);
    }

    @Test
    public void findByCategoryTypeTest(){
        List<Integer> list = Arrays.asList(1,3,4);
        List<ProductCategory> productCategories = repository.findByCategoryTypeIn(list);
        Assert.assertNotEquals(0,list.size());
    }
}