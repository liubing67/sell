package com.snail.sell.repository;

import com.snail.sell.dataobject.SellerInfo;
import com.snail.sell.utils.KeyUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:04 2017/12/18
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerInfoRepositoryTest {

   @Autowired
   private SellerInfoRepository sellerInfoRepository;
   @Test
   public void save(){
       SellerInfo sellerInfo = new SellerInfo();
       sellerInfo.setSellerId(KeyUtils.genUniqueKey());
       sellerInfo.setUsername("root");
       sellerInfo.setPassword("123456");
       sellerInfo.setOpenid("123");
       SellerInfo result = sellerInfoRepository.save(sellerInfo);
       Assert.assertNotNull(result);
   }

   @Test
    public void findByOpenid(){
        SellerInfo sellerInfo = sellerInfoRepository.findByOpenid("123");
        Assert.assertNotNull(sellerInfo);
    }

}