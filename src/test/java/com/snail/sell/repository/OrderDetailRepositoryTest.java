package com.snail.sell.repository;

import com.snail.sell.dataobject.OrderDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:01 2017/12/3
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderDetailRepositoryTest {
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Test
    public void saveTest(){
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setDetailId("1212");
        orderDetail.setOrderId("123");
        orderDetail.setProductId("1221");
        orderDetail.setProductIcon("1111.jpg");
        orderDetail.setProductPrice(new BigDecimal(20.20));
        orderDetail.setProductQuantity(100);
        orderDetail.setProductName("抹茶蛋糕");
        OrderDetail orderDetail1 = orderDetailRepository.save(orderDetail);
        Assert.assertNotNull(orderDetail);
    }

    @Test
    public void findByOrOrderId() throws Exception {
        List<OrderDetail> orderDetail = orderDetailRepository.findByOrOrderId("123");
        Assert.assertNotEquals(0,orderDetail.size());
    }

}