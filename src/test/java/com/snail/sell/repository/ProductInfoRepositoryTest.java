package com.snail.sell.repository;

import com.snail.sell.dataobject.ProductInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:20 2017/12/2
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoRepositoryTest {

    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Test
    public void saveTest(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("1");
        productInfo.setProductName("皮蛋粥");
        productInfo.setProductPrice(new BigDecimal(8.9));
        productInfo.setProductDescription("皮蛋瘦肉粥");
        productInfo.setProductStock(100);
        productInfo.setCategoryType(3);
        productInfo.setProductIcon("http://1wwdwees.jpg");
        productInfo.setProductStatus(0);
        ProductInfo result = productInfoRepository.save(productInfo);
        Assert.assertNotNull(result);

    }
    @Test
    public void findByProductStatus() throws Exception {
        List<ProductInfo> productInfoList = productInfoRepository.findByProductStatus(0);
        Assert.assertNotEquals(0,productInfoList.size());
    }

}