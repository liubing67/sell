package com.snail.sell.repository;

import com.snail.sell.dataobject.OrderMaster;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:19 2017/12/3
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterRepositoryTest {
    @Autowired
    private OrderMasterRepository orderMasterRepository;

    @Test
    public void addTest(){
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("12345");
        orderMaster.setBuyerOpenid("111111");
        orderMaster.setBuyerName("小明");
        orderMaster.setBuyerAddress("NJUPT");
        orderMaster.setBuyerPhone("18764537334");
        orderMaster.setOrderAmount(new BigDecimal(8.9));

        OrderMaster result = orderMasterRepository.save(orderMaster);

        Assert.assertNotNull(result);
    }

    @Test
    public void findByBuyerOpenid() throws Exception {
        PageRequest request = new PageRequest(0,1);
        Page<OrderMaster> result=  orderMasterRepository.findByBuyerOpenid("111111",request);
        System.out.println("+++++++++++++"+result.getTotalElements());
        Assert.assertNotEquals(0,result.getTotalElements());
    }

}