package com.snail.sell.service.Impl;

import com.snail.sell.dataobject.SellerInfo;
import com.snail.sell.service.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:25 2017/12/18
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SellerServiceImplTest {
    private static final String openid="123";

    @Autowired
    private SellerService sellerService;

    @Test
    public void findSellerInfoByOpenid() throws Exception {
        SellerInfo result = sellerService.findSellerInfoByOpenid(openid);
        Assert.assertNotNull(result);
    }

    @Test
    public void findSellerInfoByUsername(){
        SellerInfo sellerInfo = sellerService.findSellerInfoByUsername("root");
        System.out.println("+++++++++"+sellerInfo);
        Assert.assertNotNull(sellerInfo);
    }

}