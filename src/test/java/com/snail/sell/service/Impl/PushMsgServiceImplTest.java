package com.snail.sell.service.Impl;

import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.OrderStatusEnums;
import com.snail.sell.service.OrderService;
import com.snail.sell.service.PushMsgService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:40 2017/12/19
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PushMsgServiceImplTest {

    @Autowired
    private PushMsgService pushMsgService;

    @Autowired
    private OrderService orderService;


    @Test
    public void orderStatus() throws Exception {
        OrderDTO orderDTO = orderService.findOne("1512465173030346332");
        pushMsgService.orderStatus(orderDTO);

    }

}