package com.snail.sell.service.Impl;

import com.snail.sell.dataobject.ProductInfo;
import com.snail.sell.enums.ProductStatusEnums;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:10 2017/12/2
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {
    @Autowired
    private ProductService productService;

    @Test
    public void findOne() throws Exception {
        ProductInfo result = productService.findOne("1");
        Assert.assertNotNull(result);
    }

    @Test
    public void findUpAll() throws Exception {
        List<ProductInfo> productInfos = productService.findUpAll();
        Assert.assertNotEquals(0,productInfos.size());
    }

    @Test
    public void findAll() throws Exception {
        PageRequest request = new PageRequest(0,2);
        Page<ProductInfo> result = productService.findAll(request);
        System.out.printf("++++"+result.getTotalElements());
        Assert.assertNotEquals(0,result.getTotalElements());
    }

    @Test
    public void save() throws Exception {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("123457");
        productInfo.setProductName("煎饺");
        productInfo.setProductPrice(new BigDecimal(8.9));
        productInfo.setProductDescription("好吃的煎饺");
        productInfo.setProductStock(100);
        productInfo.setCategoryType(3);
        productInfo.setProductIcon("http://1wwdwees.jpg");
        productInfo.setProductStatus(ProductStatusEnums.DOWN.getCode());
        ProductInfo result = productService.save(productInfo);
        Assert.assertNotNull(result);

    }

    @Test
    public void onSale(){
        ProductInfo productInfo = productService.onSale("1221");
        Assert.assertEquals(ProductStatusEnums.UP,productInfo.getProductStatusEnums());
    }

    @Test
    public void offSale(){
        ProductInfo productInfo = productService.offSale("1221");
        Assert.assertEquals(ProductStatusEnums.DOWN,productInfo.getProductStatusEnums());
    }

}