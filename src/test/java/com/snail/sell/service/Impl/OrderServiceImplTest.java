package com.snail.sell.service.Impl;

import com.snail.sell.dataobject.OrderDetail;
import com.snail.sell.dataobject.OrderMaster;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.OrderStatusEnums;
import com.snail.sell.enums.PayStatusEnums;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:54 2017/12/4
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderServiceImplTest {
    public static final String orderId = "1512383139447962953";

    public static final String buyerOpenid = "qwqqwqwqqwqwq";

    public static final String ORDER_ID = "1512383139447962953";

    @Autowired
    private OrderService orderService;

    @Test
    public void createOrder() throws Exception {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("XJLll");
        orderDTO.setBuyerAddress("NJUPT");
        orderDTO.setBuyerPhone("111111111");
        orderDTO.setBuyerOpenid("qwqqwqwqqwqwq");
        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail o1 = new OrderDetail();
        o1.setProductId("1221");
        o1.setProductQuantity(1);

        OrderDetail o2 = new OrderDetail();
        o2.setProductId("123457");
        o2.setProductQuantity(2);

        orderDetailList.add(o1);
        orderDetailList.add(o2);
        orderDTO.setOrderDetailList(orderDetailList);

        OrderDTO result = orderService.createOrder(orderDTO);
        log.info("【创建订单】 resut=",result);
    }

    @Test
    public void findOne() throws Exception {
        OrderDTO orderDTO = orderService.findOne(orderId);
        log.info("【查询单个订单】 orderDTO",orderDTO);
        Assert.assertEquals(orderId ,orderDTO.getOrderId());


    }

    @Test
    public void findOrderList() throws Exception {
        PageRequest pageRequest = new PageRequest(0,2);
        Page<OrderDTO> orderMasterPage = orderService.findOrderList(buyerOpenid,pageRequest);
        Assert.assertNotEquals(0,orderMasterPage.getTotalElements());
    }

    @Test
    public void cancle() throws Exception {

        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.cancle(orderDTO);
        Assert.assertEquals(OrderStatusEnums.CANCLE.getCode(),result.getOrderStatus());
    }

    @Test
    public void finish() throws Exception {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.finish(orderDTO);
        Assert.assertEquals(OrderStatusEnums.FINISHED.getCode(),result.getOrderStatus());

    }

    @Test
    public void pay() throws Exception {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.pay(orderDTO);
        Assert.assertEquals(PayStatusEnums.PAY.getCode(),result.getPayStatus());

    }

    @Test
    public void findAllOrderList(){
        PageRequest pageRequest = new PageRequest(0,2);
        Page<OrderDTO> orderMasterPage = orderService.findOrderList(pageRequest);
        Assert.assertNotEquals(0,orderMasterPage.getTotalElements());
    }

}