package com.snail.sell.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:27 2017/12/1
 * @Modified By:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoggerTest
{
    private final Logger logger = LoggerFactory.getLogger(LoggerTest.class);
    @Test
    public void test1() {
        String name = "snail";
        String password = "123456";
        logger.debug("debug.....");
        logger.info("info......");
        logger.error("error.....");

    }
}
