<html>
<head>
    <meta charset="UTF-8">
    <title>支付成功</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/sell/css/style.css">

</head>
<body>
<div id="wrapper" class="toggled">
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <div class="alert alert-dismissable alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>
                            成功!
                        </h4> <strong>${msg!""} </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>