<html>
    <#include "../common/head.ftl">


    <body>

    <div id="wrapper" class="toggled">
        <#include "../common/nav.ftl">

        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-8 column">
                        <form role="form" method="post" action="/sell/seller/category/save">
                            <#--<div class="form-group">
                                <label>类目ID</label>
                                <input name="categoryId" value="${(productCategory.categoryId)!''}" type="number" class="form-control" />
                            </div>-->

                            <div class="form-group">
                                <label>type</label>
                                <input name="categoryType" value="${(productCategory.categoryType)!''}" type="number" class="form-control" />
                            </div>

                            <div class="form-group">
                                <label>类目名称</label>
                                <input name="categoryName" value="${(productCategory.categoryName)!''}" type="text" class="form-control" />
                            </div>


                            <input hidden name="categoryId" value="${(productCategory.categoryId)!''}">

                             <button type="submit" class="btn btn-default btn-primary">提交</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
