<html>
    <#include "../common/head.ftl">


    <body>

    <div id="wrapper" class="toggled">
        <#include "../common/nav.ftl">

        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-8 column">
                        <form role="form" method="post" action="/sell/seller/product/save">
                            <div class="form-group">
                                <label>商品名称</label>
                                <input name="productName" value="${(productInfo.productName)!''}" type="text" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>商品价格</label>
                                <input name="productPrice" value="${(productInfo.productPrice)!''}" type="text" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="productStock">商品库存</label>
                                <input name="productStock" value="${(productInfo.productStock)!''}" type="number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="productDescription">商品描述</label>
                                <input type="text" name="productDescription" value="${(productInfo.productDescription)!''}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>商品图片</label>
                                <img width="100" height="100" src="${(productInfo.productIcon)!'/sell/pictures/default.jpg'}" alt="">
                                <input type="text" name="productIcon" value="${(productInfo.productIcon)!'/sell/pictures/default.jpg'}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>商品类目</label>
                                <select name="categoryType" class="form-control">
                                    <#list productCategoryList as productCategory>
                                        <option value="${productCategory.categoryType}"
                                        <#if (productInfo.categoryType)??&&productInfo.categoryType==productCategory.categoryType>
                                        selected

                                        </#if>
                                        >${productCategory.categoryName}
                                        </option>
                                    </#list>

                                </select>
                            </div>
                            <input hidden name="productId" value="${(productInfo.productId)!''}">

                             <button type="submit" class="btn btn-default btn-primary">提交</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
