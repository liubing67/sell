package com.snail.sell.Exception;

import com.snail.sell.enums.ResultEnums;
import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 15:02 2017/12/4
 * @Modified By:
 **/
@Getter
public class SellException extends RuntimeException {

    private Integer code;

    public SellException(ResultEnums resultEnums){
        super(resultEnums.getMsg());
        this.code = resultEnums.getCode();
    }
    public SellException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
