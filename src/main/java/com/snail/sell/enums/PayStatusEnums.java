package com.snail.sell.enums;

import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:26 2017/12/3
 * @Modified By:
 **/
@Getter
public enum PayStatusEnums implements CodeEnum {
    PAY(1,"已支付"),
    UNPAY(0,"未支付")
    ;

    private Integer code;

    private String msg;

    PayStatusEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
