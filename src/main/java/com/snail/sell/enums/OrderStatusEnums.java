package com.snail.sell.enums;

import com.snail.sell.utils.EnumUtils;
import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:23 2017/12/3
 * @Modified By:
 **/
@Getter
public enum OrderStatusEnums implements CodeEnum{
    NEW(0,"新订单"),
    FINISHED(1,"已完结"),
    CANCLE(2,"已取消")
    ;

    private Integer code;

    private String msg;

    OrderStatusEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
