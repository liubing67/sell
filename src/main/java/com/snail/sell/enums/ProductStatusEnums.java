package com.snail.sell.enums;

import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:03 2017/12/2
 * @Modified By:
 **/
@Getter
public enum ProductStatusEnums implements CodeEnum{
    UP(0,"上架"),
    DOWN(1,"下架")
    ;

    private Integer code;

    private String msg;

    ProductStatusEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
