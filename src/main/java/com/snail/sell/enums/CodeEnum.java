package com.snail.sell.enums;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:10 2017/12/16
 * @Modified By:
 **/
public interface CodeEnum {
    Integer getCode();
}
