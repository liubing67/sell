package com.snail.sell.enums;

import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 15:04 2017/12/4
 * @Modified By:
 **/
@Getter
public enum ResultEnums {

    SUCCESS(0,"成功"),
    PARAM_ERROR(1,"参数错误"),
    PRODUCT_NOT_EXIST(10,"商品不存在"),
    PRODUCT_STOCK_ERROR(11,"商品库存不正确"),
    ORDER_NOT_EXIST(12,"订单不存在"),
    ORDER_DETAIL_NOT_EXITST(13,"订单详情不存在"),
    ORDER_STATUS_ERROR(14,"订单状态错误"),
    ORDER_DETAIL_NULL(15,"订单详情为空"),
    ORDER_UPDATE_FAILURE(16,"订单更新失败"),
    ORDER_PAY_STATUS_ERROR(17,"订单支付状态错误"),
    CART_EMPTY(18,"购物车为空"),
    ORDER_SAVE_ERROR(19,"订单保存错误"),
    ORDER_OWNER_ERROR(20,"该订单不属于当前用户"),
    WECHAT_MP_ERROR(21,"微信网页方面异常"),
    WECHAT_NOTIFY_MONEY_VERIFY_ERROR(22,"微信支付金额验证错误"),
    ORDER_CANCEL_SUCCESS(22,"订单取消成功"),
    ORDER_FINISH_SUCCESS(23,"订单完结成功"),
    PRODUCT_STATUS_ERROR(24,"商品状态错误"),
    PRODUCT_ID_NULL(25,"商品属性为空"),
    LOGIN_ERROR(26,"登陆失败，登陆信息不正确"),
    LOGOUT_SUCCESS(27,"登出成功")

    ;
    private Integer code;

    private String msg;

    ResultEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
