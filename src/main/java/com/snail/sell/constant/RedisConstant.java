package com.snail.sell.constant;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 13:55 2017/12/19
 * @Modified By:
 **/

public interface RedisConstant {

    String TOKEN_PREFIX = "token_%s";
    Integer EXPIRE = 7200;
}
