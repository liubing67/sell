package com.snail.sell.constant;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:09 2017/12/19
 * @Modified By:
 **/
public interface CookieConstant {
    String TOKEN = "token";

    Integer EXPIRE = 7200;
}
