package com.snail.sell.utils;

import com.snail.sell.enums.CodeEnum;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:14 2017/12/16
 * @Modified By:
 **/
public class EnumUtils {
    public static <T extends CodeEnum>T getByCode(Integer code, Class<T> enumClass){
        for (T each:enumClass.getEnumConstants()) {
            if (code.equals(each.getCode())){
                return each;
            }

        }
        return null;
    }

}
