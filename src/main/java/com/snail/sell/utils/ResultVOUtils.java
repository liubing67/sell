package com.snail.sell.utils;

import com.snail.sell.vo.ResultVO;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:17 2017/12/3
 * @Modified By:
 **/
public class ResultVOUtils {

    public static ResultVO success(Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setData(object);
        resultVO.setCode(0);
        resultVO.setMsg("成功");
        return resultVO;
    }

    public static ResultVO success(){
        return success(null);
    }

    public static ResultVO failure(String msg,Integer code){
        ResultVO resultVO = new ResultVO();
        resultVO.setMsg(msg);
        resultVO.setCode(code);
        return  resultVO;
    }
}
