package com.snail.sell.utils;

import java.util.Random;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 15:33 2017/12/4
 * @Modified By:
 **/
public class KeyUtils {

    public static synchronized String genUniqueKey(){
        Random random = new Random();
        Integer number = random.nextInt(900000)+100000;
        return System.currentTimeMillis()+String.valueOf(number);
    }
}
