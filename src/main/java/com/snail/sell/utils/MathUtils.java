package com.snail.sell.utils;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:18 2017/12/15
 * @Modified By:
 **/
public class MathUtils {
    private static final Double Math_Range=0.01;
    public static Boolean equals(Double responseNum, Double orderNum){

        Double result = Math.abs(responseNum-orderNum);
        if (result<Math_Range){
            return true;
        }else {
            return false;
        }
    }
}
