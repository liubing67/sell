package com.snail.sell.utils.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:55 2017/12/14
 * @Modified By:
 **/
public class JsonUtils {

    private static String toJson(Object object){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = new Gson();


        return gson.toJson(object);
    }
}
