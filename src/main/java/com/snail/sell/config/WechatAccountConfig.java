package com.snail.sell.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 19:54 2017/12/11
 * @Modified By:
 **/
@Data
@Component
@ConfigurationProperties(prefix = "wechat")
public class WechatAccountConfig {

    private String mpAppId;

    private String mpAppSecret;

    /**
     * 商户Id
     */
    private String mchId;

    /**
     * 商户密匙
     */
    private String mchKey;
    /**
     * 商户证书路径
     */
    private String keyPath;

    /**
     * 支付异步通知地址
     */
    private String nofify;

    /**
     * 微信模板Id
     *
     */
    private Map<String,String> templateId;


}
