package com.snail.sell.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.websocket.server.ServerEndpointConfig;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 15:47 2017/12/20
 * @Modified By:
 **/

@Component
public class WeSocketConfig
{
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}

