package com.snail.sell.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:17 2017/12/19
 * @Modified By:
 **/
@Data
@ConfigurationProperties(prefix = "projectUrl")
@Component
public class ProjectConfig {

    private String wechatMpAuthorize;

    private String wechatOpenAuthorize;

    private String sell;

}
