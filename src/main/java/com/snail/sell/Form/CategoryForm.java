package com.snail.sell.Form;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 19:49 2017/12/18
 * @Modified By:
 **/
@Data
public class CategoryForm {

    private Integer categoryId;

    private String categoryName;

    private Integer categoryType;
}
