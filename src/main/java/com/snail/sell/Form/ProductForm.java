package com.snail.sell.Form;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 15:59 2017/12/18
 * @Modified By:
 **/
@Data
public class ProductForm {

    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private Integer productStock;
    private String productDescription;
    private String productIcon;
    private Integer categoryType;

}
