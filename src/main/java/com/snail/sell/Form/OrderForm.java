package com.snail.sell.Form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:53 2017/12/5
 * @Modified By:
 **/
@Data
public class OrderForm {
    /**
     * 买家姓名
     */
    @NotEmpty(message = "姓名必填")
    private String name;

    /**
     * 买家地址
     */
    @NotEmpty(message = "地址必填")
    private String address;

    /**
     * 买家手机
     */
    @NotEmpty(message = "手机必填")
    private String phone;

    /**
     * 买家微信
     */
    @NotEmpty(message = "微信id必填")
    private String openid;

    /**
     * 购物车
     */
    @NotEmpty(message = "购物车不能为空")
    private String items;
}
