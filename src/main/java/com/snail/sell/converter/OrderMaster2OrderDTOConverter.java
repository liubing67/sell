package com.snail.sell.converter;

import com.snail.sell.dataobject.OrderMaster;
import com.snail.sell.dto.OrderDTO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:54 2017/12/4
 * @Modified By:
 **/
public class OrderMaster2OrderDTOConverter {

    public static OrderDTO converter(OrderMaster orderMaster){
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(orderMaster,orderDTO);
        return orderDTO;
    }

    public static List<OrderDTO> converter(List<OrderMaster> orderMasterList){
        List<OrderDTO> orderDTOList = new ArrayList<>();

        orderDTOList = orderMasterList.stream().map(e -> converter(e)).collect(Collectors.toList());

        return orderDTOList;
    }
}
