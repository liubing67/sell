package com.snail.sell.handler;

import com.snail.sell.Exception.SellException;
import com.snail.sell.Exception.SellerAuthorizeException;
import com.snail.sell.config.ProjectConfig;
import com.snail.sell.controller.SellerUserController;
import com.snail.sell.utils.ResultVOUtils;
import com.snail.sell.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:51 2017/12/19
 * @Modified By:
 **/
@ControllerAdvice
public class SellerExceptionHandler {
    @Autowired
    private ProjectConfig projectConfig;

    //拦截登陆异常
    @ExceptionHandler(value = SellerAuthorizeException.class)
    public ModelAndView handlerAuthorizeException(){

        return new ModelAndView("jsp/login");
    }

    @ExceptionHandler(value = SellException.class)
    @ResponseBody
    public ResultVO handlerSellerException(SellException e){
       return ResultVOUtils.failure(e.getMessage(),e.getCode());
    }
}
