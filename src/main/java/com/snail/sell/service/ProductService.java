package com.snail.sell.service;

import com.snail.sell.dataobject.ProductInfo;
import com.snail.sell.dto.CartDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:55 2017/12/2
 * @Modified By:
 **/
public interface ProductService {

    ProductInfo findOne(String productid);

    /**
     * 查找所有在架商品
     * @return
     */
    List<ProductInfo> findUpAll();

    Page<ProductInfo> findAll(Pageable pageable);

    ProductInfo save(ProductInfo productInfo);

    //减库存
    void decreaseStock(List<CartDTO> cartDTOList);

    //加库存
    void increaseStock(List<CartDTO> cartDTOList);

    //商品上架
    ProductInfo onSale(String productId);

    //商品下架
    ProductInfo offSale(String productId);
}
