package com.snail.sell.service.Impl;

import com.snail.sell.Exception.SellException;
import com.snail.sell.dataobject.ProductInfo;
import com.snail.sell.dto.CartDTO;
import com.snail.sell.enums.ProductStatusEnums;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.repository.ProductInfoRepository;
import com.snail.sell.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:59 2017/12/2
 * @Modified By:
 **/
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Override
    public ProductInfo findOne(String productid) {
        /*if (productid==null){
            throw new SellException(ResultEnums.PRODUCT_ID_NULL);
        }*/
        ProductInfo productInfo = productInfoRepository.findOne(productid);

        return productInfo;
    }

    @Override
    public List<ProductInfo> findUpAll() {
        List<ProductInfo> productInfos = productInfoRepository.findByProductStatus(ProductStatusEnums.UP.getCode());
        return productInfos;
    }

    @Override
    public Page<ProductInfo> findAll(Pageable pageable) {

        Page<ProductInfo> productInfos = productInfoRepository.findAll(pageable);

        return productInfos;
    }

    @Override
    public ProductInfo save(ProductInfo productInfo) {
        ProductInfo result = productInfoRepository.save(productInfo);
        return result;
    }

    @Override
    @Transactional
    public void decreaseStock(List<CartDTO> cartDTOList) {
        for (CartDTO cartDTO:cartDTOList){
            ProductInfo productInfo = productInfoRepository.findOne(cartDTO.getProductId());
            if (productInfo==null){
                throw new SellException(ResultEnums.PRODUCT_NOT_EXIST);
            }
            Integer stock = productInfo.getProductStock()-cartDTO.getProductQuantity();
            if (stock<0){
                throw new SellException(ResultEnums.PRODUCT_STOCK_ERROR);
            }
            productInfo.setProductStock(stock);
            productInfoRepository.save(productInfo);
        }
    }

    @Override
    @Transactional
    public void increaseStock(List<CartDTO> cartDTOList) {
        for (CartDTO cartDTO:cartDTOList){
            ProductInfo productInfo = productInfoRepository.findOne(cartDTO.getProductId());
            if (productInfo==null){
                throw new SellException(ResultEnums.PRODUCT_NOT_EXIST);
            }
            Integer stock = productInfo.getProductStock() + cartDTO.getProductQuantity();
            productInfo.setProductStock(stock);
            productInfoRepository.save(productInfo);
        }

    }

    @Override
    public ProductInfo onSale(String productId) {
        ProductInfo productInfo = productInfoRepository.findOne(productId);
        if (productInfo==null){
            throw new SellException(ResultEnums.PRODUCT_NOT_EXIST);
        }
        if (productInfo.getProductStatus()==ProductStatusEnums.UP.getCode()){
            throw new SellException(ResultEnums.PRODUCT_STATUS_ERROR);
        }
        productInfo.setProductStatus(ProductStatusEnums.UP.getCode());
        return productInfoRepository.save(productInfo);
    }

    @Override
    public ProductInfo offSale(String productId) {
        ProductInfo productInfo = productInfoRepository.findOne(productId);
        if (productInfo==null){
            throw new SellException(ResultEnums.PRODUCT_NOT_EXIST);
        }
        if (productInfo.getProductStatus()==ProductStatusEnums.DOWN.getCode()){
            throw new SellException(ResultEnums.PRODUCT_STATUS_ERROR);
        }
        productInfo.setProductStatus(ProductStatusEnums.DOWN.getCode());
        return productInfoRepository.save(productInfo);
    }
}
