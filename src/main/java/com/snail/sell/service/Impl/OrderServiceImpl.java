package com.snail.sell.service.Impl;

import com.snail.sell.Exception.SellException;
import com.snail.sell.converter.OrderMaster2OrderDTOConverter;
import com.snail.sell.dataobject.OrderDetail;
import com.snail.sell.dataobject.OrderMaster;
import com.snail.sell.dataobject.ProductInfo;
import com.snail.sell.dto.CartDTO;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.OrderStatusEnums;
import com.snail.sell.enums.PayStatusEnums;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.repository.OrderDetailRepository;
import com.snail.sell.repository.OrderMasterRepository;
import com.snail.sell.service.*;
import com.snail.sell.utils.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:35 2017/12/4
 * @Modified By:
 **/
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderMasterRepository orderMasterRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private PayService payService;

    @Autowired
    private PushMsgService pushMsgService;

    @Autowired
    private WebSocket webSocket;

    @Override
    @Transactional
    public OrderDTO createOrder(OrderDTO orderDTO) {

        BigDecimal orderAmount = new BigDecimal(BigInteger.ZERO);

        String orderId = KeyUtils.genUniqueKey();
        //查询商品（数量，价格）
        for (OrderDetail orderDetail:orderDTO.getOrderDetailList()){

            ProductInfo productInfo = productService.findOne(orderDetail.getProductId());
            if (productInfo==null){
                throw new SellException(ResultEnums.PRODUCT_NOT_EXIST);
            }
            //计算总价
            orderAmount = productInfo.getProductPrice()
                    .multiply(new BigDecimal(orderDetail.getProductQuantity()))
                    .add(orderAmount);
            orderDetail.setOrderId(orderId);
            orderDetail.setDetailId(KeyUtils.genUniqueKey());
            BeanUtils.copyProperties(productInfo,orderDetail);
            //订单详情入库
            OrderDetail result = orderDetailRepository.save(orderDetail);
            if (result==null){
                log.info("【订单新增】订单详情保存失败 orderDetail={}", orderDetail);
                throw new SellException(ResultEnums.ORDER_SAVE_ERROR);
            }

        }

        //写入订单数据库

        OrderMaster orderMaster = new OrderMaster();
        orderDTO.setOrderId(orderId);

        BeanUtils.copyProperties(orderDTO,orderMaster);

        orderMaster.setOrderAmount(orderAmount);
        orderMaster.setOrderStatus(OrderStatusEnums.NEW.getCode());
        orderMaster.setPayStatus(PayStatusEnums.UNPAY.getCode());
        OrderMaster result2 = orderMasterRepository.save(orderMaster);
        if (result2==null){
            log.info("【订单新增】订单保存失败 orderMaster={}",orderMaster);
            throw new SellException(ResultEnums.ORDER_SAVE_ERROR);
        }

        //扣库存
       List<CartDTO> cartDTOList =  orderDTO.getOrderDetailList().stream().map(e-> new CartDTO(e.getProductId(),e.getProductQuantity())).collect(Collectors.toList());
        productService.decreaseStock(cartDTOList);

        //发送websocket消息
        webSocket.sendMessage(orderDTO.getOrderId());
        return orderDTO;


    }

    @Override
    public OrderDTO findOne(String orderId) {
        OrderDTO orderDTO = new OrderDTO();
        OrderMaster orderMaster = orderMasterRepository.findOne(orderId);
        if (orderMaster==null){
            throw new SellException(ResultEnums.ORDER_NOT_EXIST);
        }
        List<OrderDetail> orderDetailList = orderDetailRepository.findByOrOrderId(orderId);
        if (CollectionUtils.isEmpty(orderDetailList)){
            throw new SellException(ResultEnums.ORDER_DETAIL_NOT_EXITST);
        }
        BeanUtils.copyProperties(orderMaster,orderDTO);
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;
    }

    @Override
    public Page<OrderDTO> findOrderList(String buyerOpenid, Pageable pageable) {
        Page<OrderMaster> orderMasterPage = orderMasterRepository.findByBuyerOpenid(buyerOpenid,pageable);
        List<OrderDTO> orderDTOList = OrderMaster2OrderDTOConverter.converter(orderMasterPage.getContent());
        Page<OrderDTO> orderDTOPage = new PageImpl<OrderDTO>(orderDTOList,pageable,orderMasterPage.getTotalElements());
        return orderDTOPage;
    }

    @Override
    @Transactional
    public OrderDTO cancle(OrderDTO orderDTO) {

        OrderMaster orderMaster = new OrderMaster();
        //查询订单状态
        if (!orderDTO.getOrderStatus().equals(OrderStatusEnums.NEW.getCode())){
            log.info("【取消订单】状态不确定 orderId={}, orderStatus={}",orderDTO.getOrderId(),orderDTO.getOrderStatus());
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);
        }
        //修改订单状态
        orderDTO.setOrderStatus(OrderStatusEnums.CANCLE.getCode());
        BeanUtils.copyProperties(orderDTO,orderMaster);
        OrderMaster result  = orderMasterRepository.save(orderMaster);
        if (result==null){
            log.info("【取消订单】i订单更新失败，orderMaster={}",orderMaster);
            throw new SellException(ResultEnums.ORDER_UPDATE_FAILURE);
        }
        //修改库存
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())){
            log.info("【取消订单】订单中无订单详情,orderDTO={}",orderDTO);
            throw new SellException(ResultEnums.ORDER_DETAIL_NULL);
        }
        List<OrderDTO> orderDTOList = new ArrayList<>();
        List<CartDTO> cartDTOList =  orderDTO.getOrderDetailList().stream().map(e ->new CartDTO(e.getProductId(),e.getProductQuantity())).collect(Collectors.toList());
        productService.increaseStock(cartDTOList);

        //如果已支付，退还金额
        if ( orderDTO.getPayStatus().equals(PayStatusEnums.PAY)){
            //
            payService.refund(orderDTO);
        }

        return orderDTO;
    }

    @Override
    @Transactional
    public OrderDTO finish(OrderDTO orderDTO) {

        //查询订单状态
        if (!orderDTO.getOrderStatus().equals(OrderStatusEnums.NEW.getCode())){
            log.info("【完结订单】 订单状态错误 orderId={} oderStatus={}",orderDTO.getOrderId(),orderDTO.getOrderStatus());
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);
        }

        //修改订单状态
        OrderMaster orderMaster = new OrderMaster();
        orderDTO.setOrderStatus(OrderStatusEnums.FINISHED.getCode());
        BeanUtils.copyProperties(orderDTO,orderMaster);

        //保存订单
        OrderMaster updateResult = orderMasterRepository.save(orderMaster);
        if (updateResult==null){
            log.info("【完结订单】 订单更新失败 orderMaster={}", orderMaster);
            throw new SellException(ResultEnums.ORDER_UPDATE_FAILURE);
        }
        //推送微信模板消息
        pushMsgService.orderStatus(orderDTO);
        return orderDTO;
    }

    @Override
    @Transactional
    public OrderDTO pay(OrderDTO orderDTO) {
        //查询订单状态,只有订单状态为 NEW 才可以进行支付
        if (!orderDTO.getOrderStatus().equals(OrderStatusEnums.NEW.getCode())){
            log.info("【订单支付】 订单状态错误 订单状态错误 orderId={}, orderStatus={}",orderDTO.getOrderId(),orderDTO.getOrderStatus());
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);
        }

        //判断支付状态
        if (!orderDTO.getPayStatus().equals(PayStatusEnums.UNPAY.getCode())){
            log.info("【订单支付】订单支付状态错误 orderDTO={}",orderDTO);
            throw new SellException(ResultEnums.ORDER_PAY_STATUS_ERROR);
        }

        //修改支付状态
        orderDTO.setPayStatus(PayStatusEnums.PAY.getCode());
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDTO,orderMaster);


        //保存
        OrderMaster updateResult = orderMasterRepository.save(orderMaster);
        if (updateResult==null){
            log.info("【订单支付】 订单支付状态更新失败 updateResult={}",updateResult);
            throw new SellException(ResultEnums.ORDER_UPDATE_FAILURE);
        }
        return orderDTO;
    }

    /**
     * 查询所有订单
     * @param pageable
     * @return
     */
    @Override
    public Page<OrderDTO> findOrderList(Pageable pageable) {

        Page<OrderMaster> orderMasterPage = orderMasterRepository.findAll(pageable);
        List<OrderDTO> orderDTOList = OrderMaster2OrderDTOConverter.converter(orderMasterPage.getContent());
        Page<OrderDTO> orderDTOPage = new PageImpl<OrderDTO>(orderDTOList,pageable,orderMasterPage.getTotalElements());
        return orderDTOPage;

    }
}
