package com.snail.sell.service.Impl;

import com.snail.sell.dataobject.ProductCategory;
import com.snail.sell.repository.ProductCategoryRepository;
import com.snail.sell.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:18 2017/12/2
 * @Modified By:
 **/
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
   private ProductCategoryRepository productCategoryRepository;

    @Override
    public ProductCategory findOne(Integer id) {
        ProductCategory productCategory = productCategoryRepository.findOne(id);
        return productCategory;
    }

    @Override
    public List<ProductCategory> findByCategoryTypeIn(List<Integer> list) {
        List<ProductCategory> productCategories = productCategoryRepository.findByCategoryTypeIn(list);
        return productCategories;
    }

    @Override
    public List<ProductCategory> findAll() {
       List<ProductCategory> productCategories = productCategoryRepository.findAll();
        return productCategories;
    }

    @Override
    public ProductCategory save(ProductCategory productCategory) {
       ProductCategory productCategory1 = productCategoryRepository.save(productCategory);
        return productCategory1;
    }
}
