package com.snail.sell.service.Impl;

import com.snail.sell.config.WechatAccountConfig;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.service.PushMsgService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:23 2017/12/19
 * @Modified By:
 **/
@Service
@Slf4j
public class PushMsgServiceImpl implements PushMsgService{

    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private WechatAccountConfig wechatAccountConfig;
    @Override
    public void orderStatus(OrderDTO orderDTO) {

        //Cookie: openid=o97tn1TR6883y7GKzEiwBSGo9g0w
        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(wechatAccountConfig.getTemplateId().get("orderStatus"));
        //接收消息者的openid
        wxMpTemplateMessage.setToUser(orderDTO.getBuyerOpenid());

        List<WxMpTemplateData> data = Arrays.asList(
                new WxMpTemplateData("first","亲！记得收货！"),
                new WxMpTemplateData("keyword1","微信点餐"),
                new WxMpTemplateData("keyword2","12122223334"),
                new WxMpTemplateData("keyword3",orderDTO.getBuyerOpenid()),
                new WxMpTemplateData("keyword4",orderDTO.getOrderStatusEnums().getMsg()),
                new WxMpTemplateData("keyword5","$"+orderDTO.getOrderAmount()),
                new WxMpTemplateData("remark","欢迎再次光临！")
        );
        wxMpTemplateMessage.setData(data);
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
        } catch (WxErrorException e) {
            e.printStackTrace();
            log.error("【微信模板消息】发送失败，{}",e);
        }
    }
}
