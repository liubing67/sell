package com.snail.sell.service.Impl;

import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayRequest;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundRequest;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import com.lly835.bestpay.service.impl.WxPayServiceImpl;
import com.lly835.bestpay.utils.JsonUtil;
import com.snail.sell.Exception.SellException;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.OrderService;
import com.snail.sell.service.PayService;
import com.snail.sell.utils.MathUtils;
import com.snail.sell.utils.serializer.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:11 2017/12/12
 * @Modified By:
 **/
@Service
@Slf4j
public class PayServiceImpl implements PayService {

    @Autowired
    private BestPayServiceImpl bestPayService;
    @Autowired
    private OrderService orderService;
    @Override
    public PayResponse create(OrderDTO orderDTO) {
        PayRequest request = new PayRequest();
        request.setOpenid(orderDTO.getBuyerOpenid());
        request.setOrderId(orderDTO.getOrderId());
        request.setOrderAmount(orderDTO.getOrderAmount().doubleValue());
        request.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信支付】request={}",request);
       PayResponse payResponse = bestPayService.pay(request);
       log.info("【微信支付】payRespons={}",payResponse);
        return payResponse;
    }

    @Override
    public PayResponse notify(String notifyData) {
        //验证签名
        //支付状态
        //验证金额
        //支付人==下单人？
        PayResponse payResponse = bestPayService.asyncNotify(notifyData);
        log.info("【微信支付】异步通知 payResponse={}", JsonUtil.toJson(payResponse));

        //查询订单
        OrderDTO orderDTO = orderService.findOne(payResponse.getOrderId());
        //判断订单是否存在
        if (orderDTO==null){
            log.error("【订单支付】异步通知 订单不存在 orderId={}",orderDTO.getOrderId());
            throw new SellException(ResultEnums.ORDER_NOT_EXIST);
        }
        //判断金额是否相等
        if (!MathUtils.equals(payResponse.getOrderAmount(),orderDTO.getOrderAmount().doubleValue())){
            log.error("【订单支付】异步通知 订单金额不一致, orderId={},微信通知金额={}，系统金额={}",orderDTO.getOrderId(),
                    payResponse.getOrderAmount(),
                    orderDTO.getOrderAmount());
            throw new SellException(ResultEnums.WECHAT_NOTIFY_MONEY_VERIFY_ERROR);
        }
        orderService.pay(orderDTO);
        return payResponse;
    }

    @Override
    public RefundResponse refund(OrderDTO orderDTO) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId(orderDTO.getOrderId());
        refundRequest.setOrderAmount(orderDTO.getOrderAmount().doubleValue());
        refundRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信退款】request={}",refundRequest);

        RefundResponse refundResponse = bestPayService.refund(refundRequest);
        log.info("【微信退款】response={}",refundResponse);
        return refundResponse;
    }
}
