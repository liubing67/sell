package com.snail.sell.service.Impl;

import com.snail.sell.Exception.SellException;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.BuyerService;
import com.snail.sell.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:23 2017/12/5
 * @Modified By:
 **/
@Service
@Slf4j
public class BuyerServiceImpl implements BuyerService {
    @Autowired
    private OrderService orderService;

    @Override
    public OrderDTO findOrderOne(String openid, String orderId) {
        OrderDTO orderDTO = checkOrderOwner(openid, orderId);

        return orderDTO;
    }

    @Override
    public OrderDTO cancelOrder(String openid, String orderId) {
        OrderDTO orderDTO = checkOrderOwner(openid, orderId);

        return orderService.cancle(orderDTO);
    }

    public OrderDTO checkOrderOwner(String openid,String orderId){
        OrderDTO orderDTO = orderService.findOne(orderId);
        if (orderDTO==null){
            log.info("【订单查询】 订单查询错误");
            throw new SellException(ResultEnums.ORDER_NOT_EXIST);
        }
        if (!orderDTO.getBuyerOpenid().equals(openid)){
            log.info("【查询订单】订单的openid不一致. openid={}, orderDTO={} ",openid,orderDTO);
            throw new SellException(ResultEnums.ORDER_OWNER_ERROR);
        }

        return orderDTO;
    }
}
