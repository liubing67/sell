package com.snail.sell.service.Impl;

import com.snail.sell.dataobject.SellerInfo;
import com.snail.sell.repository.SellerInfoRepository;
import com.snail.sell.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:24 2017/12/18
 * @Modified By:
 **/
@Service
public class SellerServiceImpl implements SellerService {

    @Autowired
    private SellerInfoRepository sellerInfoRepository;

    @Override
    public SellerInfo findSellerInfoByOpenid(String openid) {
        SellerInfo sellerInfo = sellerInfoRepository.findByOpenid(openid);
        return sellerInfo;
    }

    @Override
    public SellerInfo findSellerInfoByUsername(String username) {
        SellerInfo sellerInfo = sellerInfoRepository.findByUsername(username);
        return sellerInfo;
    }
}
