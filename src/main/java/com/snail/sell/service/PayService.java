package com.snail.sell.service;

import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;
import com.snail.sell.dto.OrderDTO;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:08 2017/12/12
 * @Modified By:
 **/
public interface PayService {
    PayResponse create(OrderDTO orderDTO );

    PayResponse notify(String notifyData);

    RefundResponse refund(OrderDTO orderDTO);

}
