package com.snail.sell.service;

import com.snail.sell.dataobject.SellerInfo;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:22 2017/12/18
 * @Modified By:
 **/
public interface SellerService {
    SellerInfo findSellerInfoByOpenid(String openid);

    SellerInfo findSellerInfoByUsername(String username);
}
