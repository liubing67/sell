package com.snail.sell.service;

import com.snail.sell.dto.OrderDTO;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:20 2017/12/5
 * @Modified By:
 **/
public interface BuyerService {

    OrderDTO findOrderOne(String openid,String orderId);

    OrderDTO cancelOrder(String openid,String orderId);
}
