package com.snail.sell.service;

import com.snail.sell.dataobject.ProductCategory;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:15 2017/12/2
 * @Modified By:
 **/
public interface CategoryService {
    ProductCategory findOne(Integer id);

    List<ProductCategory> findByCategoryTypeIn(List<Integer> list);

    List<ProductCategory> findAll();

    ProductCategory save(ProductCategory productCategory);
}
