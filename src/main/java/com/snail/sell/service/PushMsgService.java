package com.snail.sell.service;

import com.snail.sell.dto.OrderDTO;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:22 2017/12/19
 * @Modified By:
 **/
public interface PushMsgService {

    void orderStatus(OrderDTO orderDTO);

}
