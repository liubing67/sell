package com.snail.sell.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: XJL
 * @Description:商品详情
 * @Date: Create in 11:36 2017/12/3
 * @Modified By:
 **/
@Data
public class ProductInfoVO implements Serializable{

    private static final long serialVersionUID = -7539558105099167727L;

    @JsonProperty("id")
    private String productId;
    @JsonProperty("name")
    private String productName;
    @JsonProperty("price")
    private BigDecimal productPrice;
    @JsonProperty("description")
    private String productDescription;
    @JsonProperty("icon")
    private String productIcon;
}
