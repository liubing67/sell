package com.snail.sell.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: XJL
 * @Description:商品，包含类目
 * @Date: Create in 11:34 2017/12/3
 * @Modified By:
 **/
@Data
public class ProductVO implements Serializable{

    private static final long serialVersionUID = -1103176438825424529L;
    @JsonProperty("name")
    private String categoryName;
    @JsonProperty("type")
    private Integer categoryType;
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVOList;
}
