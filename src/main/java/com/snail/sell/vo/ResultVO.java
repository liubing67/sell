package com.snail.sell.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 11:11 2017/12/3
 * @Modified By:
 **/
@Data
public class ResultVO<T> implements Serializable{
    private static final long serialVersionUID = -1426448393163203739L;
    private Integer code;

    private String msg;

    private T data;

}
