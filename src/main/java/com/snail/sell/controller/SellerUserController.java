package com.snail.sell.controller;

import com.snail.sell.config.ProjectConfig;
import com.snail.sell.constant.CookieConstant;
import com.snail.sell.constant.RedisConstant;
import com.snail.sell.dataobject.SellerInfo;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.SellerService;
import com.snail.sell.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 13:20 2017/12/19
 * @Modified By:
 **/
@Controller
@RequestMapping("/seller")
public class SellerUserController {
    @Autowired
    private SellerService sellerService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ProjectConfig projectConfig;


    @PostMapping("/login")
    public ModelAndView login(/*@RequestParam("openid") String openid*/
                              @RequestParam("username") String username,
                              @RequestParam("password") String password,
                              HttpServletResponse response,
                              Map<String,Object> map){

        //1.openid和数据库中的进行匹配
        SellerInfo sellerInfo = sellerService.findSellerInfoByUsername(username);
        if (sellerInfo==null){
            map.put("msg", ResultEnums.LOGIN_ERROR.getMsg());
            map.put("url","/sell/seller/order/list");
            return new ModelAndView("jsp/error",map);
        }
        if (sellerInfo.getPassword().equals(password)){
            //2.设置token到redis
            String token = UUID.randomUUID().toString();
            Integer expire = RedisConstant.EXPIRE;

            redisTemplate.opsForValue().set(String.format(RedisConstant.TOKEN_PREFIX,token),username,expire, TimeUnit.SECONDS);

            //3.设置token到cookie

            CookieUtil.set(response, CookieConstant.TOKEN,token,CookieConstant.EXPIRE);
            return new ModelAndView("redirect:"+projectConfig.getSell()+"/sell/seller/order/list");
        }
       map.put("msg",ResultEnums.LOGIN_ERROR.getMsg());
        map.put("url","/sell/seller/order/list");
        return new ModelAndView("jsp/error");
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletResponse response,
                               HttpServletRequest request,
                               Map<String,Object> map){

        Cookie cookie = CookieUtil.get(request,CookieConstant.TOKEN);
        if (cookie!=null){
            //清楚redis
            redisTemplate.opsForValue().getOperations().delete(String.format(RedisConstant.TOKEN_PREFIX,cookie.getValue()));
            //清楚Cookie
            CookieUtil.set(response,CookieConstant.TOKEN,null,0);
        }
        map.put("url","http://xujiliang1.55555.io/sell/login");
        map.put("msg",ResultEnums.LOGOUT_SUCCESS.getMsg());
        return new ModelAndView("jsp/success",map);
    }
}
