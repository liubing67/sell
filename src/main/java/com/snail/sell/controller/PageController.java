package com.snail.sell.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 19:44 2017/12/19
 * @Modified By:
 **/
@Controller
public class PageController {

    @RequestMapping("/login")
    public ModelAndView login(){
        return new ModelAndView("jsp/login");
    }
}
