package com.snail.sell.controller;

import com.lly835.bestpay.model.PayResponse;
import com.snail.sell.Exception.SellException;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.OrderService;
import com.snail.sell.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:01 2017/12/12
 * @Modified By:
 **/
@Controller
@RequestMapping("/pay")
@Slf4j
public class PayController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private PayService payService;

    @RequestMapping("/create")
    public ModelAndView create(@RequestParam("orderId") String orderId,
                               @RequestParam("returnUrl") String returnUrl,
                               Map<String,Object> map){

        OrderDTO orderDTO = orderService.findOne(orderId);
        if (orderDTO==null){
            log.error("【订单查询错误，订单不存在】");
            throw new SellException(ResultEnums.ORDER_NOT_EXIST);
        }
     /*   //发起支付,调用统一下单API，生成预付单信息PayResponse
        PayResponse payResponse = payService.create(orderDTO);

        map.put("payResponse",payResponse);
        map.put("returnUrl",returnUrl);

        //微信内H5调起支付
        return new ModelAndView("pay/create",map);*/

        orderService.pay(orderDTO);
     return new ModelAndView("former/success");

    }

    /**
     * 微信异步通知
     * @param notifyData
     */
    @PostMapping("/notify")
    public ModelAndView notify(@RequestBody String notifyData){
        payService.notify(notifyData);
        return new ModelAndView("/pay/success");
    }
}
