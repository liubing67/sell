package com.snail.sell.controller;

import antlr.StringUtils;
import com.lly835.bestpay.rest.type.Get;
import com.snail.sell.Exception.SellException;
import com.snail.sell.Form.OrderForm;
import com.snail.sell.converter.OrderForm2OrderDTOConverter;
import com.snail.sell.dto.OrderDTO;
import com.snail.sell.enums.ResultEnums;
import com.snail.sell.service.BuyerService;
import com.snail.sell.service.OrderService;
import com.snail.sell.utils.ResultVOUtils;
import com.snail.sell.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:48 2017/12/5
 * @Modified By:
 **/
@RestController
@RequestMapping("/buyer/order")
@Slf4j
public class BuyerOrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BuyerService buyerService;

    //创建订单
    @PostMapping("/create")
    public ResultVO<Map<String,String>> create(@Valid OrderForm orderForm, BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            log.info("【创建订单】 参数不正确，orderForm={}",orderForm);
            throw new SellException(ResultEnums.PARAM_ERROR);
        }

        OrderDTO orderDTO = new OrderDTO();
        orderDTO = OrderForm2OrderDTOConverter.converter(orderForm);
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())){
            log.info("【创建订单】购物车为空 ");
            throw new SellException(ResultEnums.CART_EMPTY);
        }

        OrderDTO result = orderService.createOrder(orderDTO);
        Map<String,String> map = new HashMap<>();
        map.put("orderId",result.getOrderId());

        return ResultVOUtils.success(map);

    }

    //查询订单列表
    @GetMapping("/list")
    public ResultVO<List<OrderDTO>> findList(@RequestParam(value = "openid") String openid,
                                             @RequestParam(value = "page" ,defaultValue ="0") Integer page,
                                             @RequestParam(value = "size", defaultValue = "10") Integer seze){
        if (org.springframework.util.StringUtils.isEmpty(openid)){
            log.info("【查询订单列表】 openid为空");
            throw new SellException(ResultEnums.PARAM_ERROR);


        }
        PageRequest pageRequest = new PageRequest(page,seze);
        Page<OrderDTO> orderDTOPage = orderService.findOrderList(openid,pageRequest);

        return ResultVOUtils.success(orderDTOPage.getContent());
    }

    //查询订单详情
    @GetMapping("/detail")
    public ResultVO<OrderDTO> findOne(@RequestParam("openid")String openid,
                                      @RequestParam("orderId")String orderid){

        OrderDTO orderDTO = buyerService.findOrderOne(openid, orderid);

        return ResultVOUtils.success(orderDTO);
    }

    //取消订单
    @PostMapping("/cancel")
    public ResultVO cancel(@RequestParam("openid")String openid,
                           @RequestParam("orderId")String orderId){
        OrderDTO orderDTO = buyerService.cancelOrder(openid, orderId);

        return ResultVOUtils.success();
    }
}
