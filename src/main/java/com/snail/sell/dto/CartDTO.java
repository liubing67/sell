package com.snail.sell.dto;

import lombok.Data;

/**
 * @Author: XJL
 * @Description:购物车
 * @Date: Create in 16:30 2017/12/4
 * @Modified By:
 **/
@Data
public class CartDTO {

    private String productId;

    private Integer productQuantity;

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
