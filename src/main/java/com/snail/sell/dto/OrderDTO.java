package com.snail.sell.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.snail.sell.dataobject.OrderDetail;
import com.snail.sell.enums.OrderStatusEnums;
import com.snail.sell.enums.PayStatusEnums;
import com.snail.sell.utils.EnumUtils;
import com.snail.sell.utils.serializer.Date2LongSerializer;
import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 14:25 2017/12/4
 * @Modified By:
 **/
@Data
public class OrderDTO {


    private String orderId;

    private String buyerName;

    private String buyerPhone;

    private String buyerAddress;

    private String buyerOpenid;

    private BigDecimal orderAmount;

    private Integer orderStatus;

    private Integer payStatus;

    @JsonSerialize(using = Date2LongSerializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongSerializer.class)
    private Date updateTime;

    private List<OrderDetail> orderDetailList;

    @JsonIgnore
    public OrderStatusEnums getOrderStatusEnums(){
        return EnumUtils.getByCode(orderStatus,OrderStatusEnums.class);
    }

    @JsonIgnore
    public PayStatusEnums getPayStatusEnums(){
        return EnumUtils.getByCode(payStatus,PayStatusEnums.class);
    }
}
