package com.snail.sell.dataobject.mapper;

import com.snail.sell.dataobject.ProductCategory;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:12 2017/12/23
 * @Modified By:
 **/
public interface ProductCategoryMapper {

    //此处的category_name表示map的字段
    @Insert("insert into product_category(category_name, category_type) values (#{category_name, jdbcType=VARCHAR}, #{category_type, jdbcType=INTEGER})")
    int insertByMap(Map<String,Object> map);


    //此处的ctegotyName表示对象属性
    @Insert("insert into product_category(category_name, category_type) values (#{categoryName, jdbcType=VARCHAR}, #{categoryType, jdbcType=INTEGER})")
    int insertByObject(ProductCategory productCategory);


    //通过对象进行查询
    @Select("select * from product_category where category_type=#{categoryType}")
    @Results({
        @Result(column = "category_id",property = "CategoryId"),
        @Result(column = "category_name",property = "CategoryName"),
        @Result(column = "category_type",property = "CategoryType")
    }

    )
    ProductCategory findByCategoryType(Integer categoryType);

    //通过对象进行查询
    @Select("select * from product_category where category_type=#{categoryType}")
    @Results({
            @Result(column = "category_id",property = "CategoryId"),
            @Result(column = "category_name",property = "CategoryName"),
            @Result(column = "category_type",property = "CategoryType")
    }

    )
    List<ProductCategory> findByCategoryName(String CategoryName);


    @Update("update product_category set category_name = #{categoryName} where category_type=#{categoryType}")
    int uodateByCategoryType(@Param("categoryName") String categoryName,@Param("categoryType") Integer categoryType);

    //通过对象更新
    @Update("update product_category set category_name = #{categoryName} where category_type=#{categoryType}")
    int updateByObject(ProductCategory productCategory);

    @Delete("delect from product_category where category_type=#{categoryType}")
    int deleteByCategoryType(Integer categoryType);

    ProductCategory selectByCategoryType(Integer categoryType);
}
