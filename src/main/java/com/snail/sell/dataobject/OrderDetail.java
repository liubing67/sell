package com.snail.sell.dataobject;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:30 2017/12/3
 * @Modified By:
 **/
@Entity
@Data
public class OrderDetail {
    /**
     * detail_id VARCHAR(32) NOT NULL,
     order_id VARCHAR(32) NOT NULL,
     product_id VARCHAR(32) NOT NULL,
     product_name VARCHAR(64) NOT NULL COMMENT '商品名称',
     product_price DECIMAL(8,2) NOT NULL,
     product_quantity INT NOT NULL COMMENT '商品数量',
     product_icon VARCHAR(512) COMMENT '商品小图',
     create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     update_time TIMESTAMP NOT NULL  COMMENT '修改时间',
     */
    @Id
    private String detailId;

    private String orderId;

    private String productId;

    private String productName;

    private BigDecimal productPrice;

    private Integer productQuantity;

    private String productIcon;
}
