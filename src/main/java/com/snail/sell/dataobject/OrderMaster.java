package com.snail.sell.dataobject;

import com.snail.sell.enums.OrderStatusEnums;
import com.snail.sell.enums.PayStatusEnums;
import lombok.Data;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:18 2017/12/3
 * @Modified By:
 **/
@Entity
@Data
public class OrderMaster {
    /**
     * order_id VARCHAR(32) NOT NULL,
     buyer_name VARCHAR(32) NOT NULL COMMENT '买家名',
     buyer_phone VARCHAR(32) NOT NULL COMMENT '买家电话',
     buyer_address VARCHAR(128) NOT NULL COMMENT '买家地址',
     buyer_openid VARCHAR(64) NOT NULL COMMENT '买家微信openId',
     order_amount DECIMAL(8,2) NOT NULL COMMENT '订单总金额',
     order_status TINYINT(3) NOT NULL DEFAULT 0 COMMENT '订单状态，默认0新下单',
     pay_status TINYINT(3) NOT NULL DEFAULT 0 COMMENT '支付状态，默认0，表示未支付',
     create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     update_time TIMESTAMP NOT NULL  COMMENT '修改时间',
     */
    @Id
    private String orderId;

    private String buyerName;

    private String buyerPhone;

    private String buyerAddress;

    private String buyerOpenid;

    private BigDecimal orderAmount;

    private Integer orderStatus= OrderStatusEnums.NEW.getCode();

    private Integer payStatus = PayStatusEnums.UNPAY.getCode();

    private Date createTime;

    private Date updateTime;


}
