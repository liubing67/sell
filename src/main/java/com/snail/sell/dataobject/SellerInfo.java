package com.snail.sell.dataobject;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:58 2017/12/18
 * @Modified By:
 **/
@Data
@Entity
public class SellerInfo {
    @Id
    private String sellerId;

    private String password;

    private String openid;

    private String username;
}
