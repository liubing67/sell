package com.snail.sell.dataobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snail.sell.enums.ProductStatusEnums;
import com.snail.sell.utils.EnumUtils;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:14 2017/12/2
 * @Modified By:
 **/
@Entity
@DynamicUpdate
@Data
public class ProductInfo implements Serializable{
    private static final long serialVersionUID = -8708175406489244129L;
    /**
     * product_id VARCHAR(32) NOT NULL,
     product_name VARCHAR(64) NOT NULL COMMENT '商品名称',
     product_price DECIMAL(8,2) NOT NULL COMMENT '单价',
     product_stock INT NOT NULL COMMENT '库存',
     product_description VARCHAR(64) COMMENT '描述',
     product_icon VARCHAR(512) COMMENT '小图',
     product_type INT NOT NULL COMMENT '类目编号',
     create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     update_time TIMESTAMP NOT NULL  COMMENT '修改时间',
     PRIMARY KEY (product_id)
     */
    @Id
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private Integer productStock;
    private String productDescription;
    private String productIcon;
    private Integer categoryType;
    private Integer productStatus=ProductStatusEnums.UP.getCode();
    @JsonIgnore
    private Date createTime;
    @JsonIgnore
    private Date updateTime;
    public ProductStatusEnums getProductStatusEnums(){
        return EnumUtils.getByCode(productStatus,ProductStatusEnums.class);
    }

}
