package com.snail.sell.repository;

import com.snail.sell.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:16 2017/12/3
 * @Modified By:
 **/
public interface OrderMasterRepository extends JpaRepository<OrderMaster,String> {
    Page<OrderMaster> findByBuyerOpenid(String openId, Pageable pageable);
}
