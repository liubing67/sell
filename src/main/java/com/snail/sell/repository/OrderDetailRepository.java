package com.snail.sell.repository;

import com.snail.sell.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:12 2017/12/3
 * @Modified By:
 **/
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {

    List<OrderDetail> findByOrOrderId(String orderId);
}
