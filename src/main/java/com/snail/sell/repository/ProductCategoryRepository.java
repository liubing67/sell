package com.snail.sell.repository;

import com.snail.sell.dataobject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 17:14 2017/12/2
 * @Modified By:
 **/
public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer> {
    List<ProductCategory> findByCategoryTypeIn(List<Integer> list);

}
