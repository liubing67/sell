package com.snail.sell.repository;

import com.snail.sell.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:18 2017/12/2
 * @Modified By:
 **/
public interface ProductInfoRepository extends JpaRepository<ProductInfo,String> {
    List<ProductInfo> findByProductStatus(Integer status);
}
