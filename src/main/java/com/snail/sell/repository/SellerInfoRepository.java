package com.snail.sell.repository;

import com.snail.sell.dataobject.SellerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:01 2017/12/18
 * @Modified By:
 **/
public interface SellerInfoRepository extends JpaRepository<SellerInfo,String> {

    SellerInfo findByOpenid(String openid);

    SellerInfo findByUsername(String username);
}
